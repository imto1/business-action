package com.behmerd.bussinessaction;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.behmerd.bussinessaction.utils.FGService;
import com.behmerd.bussinessaction.utils.Preferences;

public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        Preferences preferences = new Preferences(getApplicationContext());

        EditText etDelay = findViewById(R.id.etDelay);
        EditText etWelcome = findViewById(R.id.etWelcome);
        Button btnSave = findViewById(R.id.btnSave);

        etDelay.setText(preferences.getPreference(preferences.DELAY_TIME));
        etWelcome.setText(preferences.getPreference(preferences.WELCOME_MESSAGE));

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!etDelay.getText().toString().equals("") &&
                        !etWelcome.getText().toString().equals("")) {
                    preferences.setPreference(preferences.DELAY_TIME,
                            etDelay.getText().toString());
                    preferences.setPreference(preferences.WELCOME_MESSAGE,
                            etWelcome.getText().toString());
                    if(!Boolean.parseBoolean(preferences.getPreference(preferences.INIT_SETTING)))
                        preferences.setPreference(preferences.INIT_SETTING, String.valueOf(true));
                    if(getIntent().getExtras() != null){
                        boolean firstLaunch = getIntent().getBooleanExtra("FIRST_LAUNCH", false);
                        if(firstLaunch){
                            Intent intent = new Intent(getApplicationContext(), FGService.class);
                            intent.setAction(FGService.ACTION_START_FOREGROUND_SERVICE);
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                                startForegroundService(intent);
                            else
                                startService(intent);
                        }
                    }
                    startActivity(new Intent(getApplicationContext(), MainActivity.class));
                } else
                    Toast.makeText(getApplicationContext(),
                            R.string.setting_toast_required, Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    protected void onStop(){
        super.onStop();
        finish();
    }
}