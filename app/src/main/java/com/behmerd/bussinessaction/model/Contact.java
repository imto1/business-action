package com.behmerd.bussinessaction.model;

import android.content.ContentValues;

import androidx.annotation.NonNull;

import com.behmerd.bussinessaction.database.DBHelper.DatabaseContract.ContactEntry;

public class Contact{

    public long id;

    @NonNull
    public String firstName;

    @NonNull
    public String lastName;

    @NonNull
    public String phone;


    public String selection = null;
    public String[] selectionArgs = null;
    public String groupBy = null;
    public String orderBy = null;

    public Contact(){}

    public Contact(Long id, @NonNull String firstName, @NonNull String lastName,
                   @NonNull String phone) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phone = phone;
    }
    public Contact(@NonNull String firstName, @NonNull String lastName,
                   @NonNull String phone) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.phone = phone;
    }
    public Contact(@NonNull ContentValues values) {
        this.firstName = values.get(ContactEntry.FIRSTNAME).toString();
        this.lastName = values.get(ContactEntry.LASTNAME).toString();
        this.phone = values.get(ContactEntry.PHONE).toString();
    }
}
