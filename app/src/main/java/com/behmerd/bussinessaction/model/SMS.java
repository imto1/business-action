package com.behmerd.bussinessaction.model;

import android.content.ContentValues;

import androidx.annotation.NonNull;

import com.behmerd.bussinessaction.database.DBHelper.DatabaseContract.SMSEntry;

public class SMS {
    public long id;

    public long sender;

    public long timeStamp;

    public String body;

    public String selection = null;
    public String[] selectionArgs = null;
    public String groupBy = null;
    public String orderBy = null;

    public SMS(){}

    public SMS(long id, long sender, @NonNull long timeStamp, @NonNull String body){
        this.id = id;
        this.sender = sender;
        this.timeStamp = timeStamp;
        this.body = body;
    }
    public SMS(long sender, @NonNull long timeStamp, @NonNull String body){
        this.sender = sender;
        this.timeStamp = timeStamp;
        this.body = body;
    }
    public SMS(@NonNull ContentValues values){
        this.sender = Long.parseLong(values.get(SMSEntry.SENDER).toString());
        this.timeStamp = Long.parseLong(values.get(SMSEntry.TIMESTAMP).toString());
        this.body = values.get(SMSEntry.BODY).toString();
    }
}
