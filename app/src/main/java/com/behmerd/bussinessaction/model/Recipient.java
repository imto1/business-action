package com.behmerd.bussinessaction.model;

import androidx.annotation.NonNull;

public class Recipient {
    public long id;
    public long receiver;

    public String selection = null;
    public String[] selectionArgs = null;
    public String groupBy = null;
    public String orderBy = null;

    public Recipient(){}

    public Recipient(@NonNull long receiver){
        this.receiver = receiver;
    }

    public Recipient(long id, @NonNull long receiver){
        this.id = id;
        this.receiver = receiver;
    }

}
