package com.behmerd.bussinessaction;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.behmerd.bussinessaction.adapters.ContactViewAdapter;
import com.behmerd.bussinessaction.database.DBHelper.DatabaseContract.ContactEntry;
import com.behmerd.bussinessaction.database.dao.ContactDao;
import com.behmerd.bussinessaction.model.Contact;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class ContactActivity extends AppCompatActivity implements ContactViewAdapter.ItemClickListener {
    private final String TAG = "CONTACT_ACTIVITY";
    private ContactViewAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);

        FloatingActionButton btnAdd = (FloatingActionButton) findViewById(R.id.btnAddContact);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),ContactItemActivity.class));
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        init();
    }

    @Override
    public void onItemClick(View view, int position) {
        Intent intent = new Intent(getApplicationContext(), ContactItemActivity.class);
        intent.putExtra(ContactEntry._ID, adapter.getItem(position).id);
        intent.putExtra(ContactEntry.FIRSTNAME, adapter.getItem(position).firstName);
        intent.putExtra(ContactEntry.LASTNAME, adapter.getItem(position).lastName);
        intent.putExtra(ContactEntry.PHONE, adapter.getItem(position).phone);
        startActivity(intent);
    }

    private void init() {
        try {
            ContactDao dao = new ContactDao(getApplicationContext());
            Cursor cursor = dao.select();
            List<Contact> contacts = new ArrayList<>();

            while (cursor.moveToNext())
                contacts.add(new Contact(
                        cursor.getLong(cursor.getColumnIndexOrThrow(ContactEntry._ID)),
                        cursor.getString(cursor.getColumnIndexOrThrow(ContactEntry.FIRSTNAME)),
                        cursor.getString(cursor.getColumnIndexOrThrow(ContactEntry.LASTNAME)),
                        cursor.getString(cursor.getColumnIndexOrThrow(ContactEntry.PHONE))
                ));

            // set up the RecyclerView
            RecyclerView recyclerView = findViewById(R.id.rvContactList);
            recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
            adapter = new ContactViewAdapter(getApplicationContext(), contacts);
            adapter.setClickListener(ContactActivity.this);
            recyclerView.setAdapter(adapter);
        } catch (Exception e) {
            Log.e(TAG, String.format("Error on contact list initialization: %s", e.getMessage()));
        }
    }

}