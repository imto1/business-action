package com.behmerd.bussinessaction;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.CountDownTimer;

import com.behmerd.bussinessaction.utils.FGService;
import com.behmerd.bussinessaction.utils.Preferences;

public class StartingActivity extends AppCompatActivity {
    final String[] permissions = {
            Manifest.permission.FOREGROUND_SERVICE,
            Manifest.permission.SEND_SMS,
            Manifest.permission.RECEIVE_SMS,
            Manifest.permission.READ_SMS
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_starting);

        if (hasPermission(Manifest.permission.FOREGROUND_SERVICE) &&
                hasPermission(Manifest.permission.SEND_SMS) &&
                hasPermission(Manifest.permission.RECEIVE_SMS) &&
                hasPermission(Manifest.permission.READ_SMS)) {
            init();
        } else {
            requestPermissionLauncher.launch(permissions);
        }
    }

    private ActivityResultLauncher<String[]> requestPermissionLauncher =
            registerForActivityResult(new ActivityResultContracts.RequestMultiplePermissions(), isGranted -> {
                if (isGranted.get(Manifest.permission.FOREGROUND_SERVICE) &&
                        isGranted.get(Manifest.permission.SEND_SMS) &&
                        isGranted.get(Manifest.permission.RECEIVE_SMS) &&
                        isGranted.get(Manifest.permission.READ_SMS)
                ) {
                    init();
                } else {
                    finish();
                }
            });

    private void init(){
        new CountDownTimer(2000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {}

            @Override
            public void onFinish() {
                Preferences preferences = new Preferences(getApplicationContext());
                boolean initSettings = Boolean.parseBoolean(preferences.getPreference(preferences.INIT_SETTING));
                if (!initSettings) {
                    Intent intent = new Intent(getApplicationContext(), SettingsActivity.class);
                    intent.putExtra("FIRST_LAUNCH", true);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(getApplicationContext(), FGService.class);
                    intent.setAction(FGService.ACTION_START_FOREGROUND_SERVICE);
                    startForegroundService(intent);

                    startActivity(new Intent(getApplicationContext(), MainActivity.class));
                }
            }
        }.start();
    }

    private boolean hasPermission(String permission)
    {
        // return permission grant status.
        int permissionCode = ContextCompat.checkSelfPermission(getApplicationContext(), permission);
        return permissionCode == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    protected void onStop(){
        super.onStop();
        finish();
    }
}