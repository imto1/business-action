package com.behmerd.bussinessaction;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentActivity;

import com.behmerd.bussinessaction.adapters.ConfirmDialog;
import com.behmerd.bussinessaction.database.DBHelper.DatabaseContract.ContactEntry;
import com.behmerd.bussinessaction.database.dao.ContactDao;
import com.behmerd.bussinessaction.database.dao.RecipientDao;
import com.behmerd.bussinessaction.model.Contact;

public class ContactItemActivity extends FragmentActivity implements ConfirmDialog.NoticeDialogListener {
    private final String TAG = "CONTACT_ITEM_ACTIVITY";

    private Contact contact;
    private boolean dialogResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_item);

        LinearLayout llEdit = findViewById(R.id.llContactEdit);
        LinearLayout llUpdate = findViewById(R.id.llContactUpdate);
        LinearLayout llAdd = findViewById(R.id.llContactAdd);

        TextView tvID = findViewById(R.id.tvCVID);
        TextView tvFirst = findViewById(R.id.tvCVFirstName);
        TextView tvLast = findViewById(R.id.tvCVLastName);
        TextView tvPhone = findViewById(R.id.tvCVPhone);

        EditText etFirst = findViewById(R.id.etCVFirstName);
        EditText etLast = findViewById(R.id.etCVLastName);
        EditText etPhone = findViewById(R.id.etCVPhone);

        Button btnAdd = findViewById(R.id.btnContactAdd);
        Button btnCancelAdd = findViewById(R.id.btnContactCancelAdd);
        Button btnUpdate = findViewById(R.id.btnContactUpdate);
        Button btnCancelEdit = findViewById(R.id.btnContactCancelEdit);
        Button btnEdit = findViewById(R.id.btnContactEdit);
        Button btnDelete = findViewById(R.id.btnContactDelete);

        if(getIntent().getExtras() != null) {
            this.contact = new Contact(
                    getIntent().getLongExtra(ContactEntry._ID,0),
                    getIntent().getStringExtra(ContactEntry.FIRSTNAME),
                    getIntent().getStringExtra(ContactEntry.LASTNAME),
                    getIntent().getStringExtra(ContactEntry.PHONE)
            );

            tvID.setText(String.format("%s: %d", getResources().getString(R.string.contact_id), contact.id));
            tvFirst.setText(String.format("%s: %s", getResources().getString(R.string.contact_first_name), contact.firstName));
            tvLast.setText(String.format("%s: %s", getResources().getString(R.string.contact_last_name), contact.lastName));
            tvPhone.setText(String.format("%s: %s", getResources().getString(R.string.contact_phone), contact.phone));

            llEdit.setVisibility(View.VISIBLE);
        } else {
            etFirst.setVisibility(View.VISIBLE);
            etLast.setVisibility(View.VISIBLE);
            etPhone.setVisibility(View.VISIBLE);
            llAdd.setVisibility(View.VISIBLE);
        }

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(etFirst.getText() != null &&
                        etLast.getText() != null &&
                        etPhone.getText() != null) {
                    ContactDao contactDao = new ContactDao(getApplicationContext(),
                            etFirst.getText().toString(),
                            etLast.getText().toString(),
                            etPhone.getText().toString());
                    long result = contactDao.insert();
                    if(result > 0) {
                        Toast.makeText(getApplicationContext(),
                                String.format("New contact inserted. ID: %d", result),
                                Toast.LENGTH_LONG).show();
                        Log.d(TAG, String.format("New contact inserted. ID: %d", result));

                        RecipientDao recipientDao = new RecipientDao(getApplicationContext(), result);
                        result = recipientDao.insert();
                        if(result > 0)
                            Log.d(TAG, String.format("New contact added to welcome list. ID: %d", result));
                        else
                            Log.e(TAG, "New contact was not added to welcome list! Something went wrong.");

                        finish();
                    } else {
                        Toast.makeText(getApplicationContext(),
                                "New contact was not inserted! Something went wrong.",
                                Toast.LENGTH_LONG).show();
                        Log.e(TAG, "New contact was not inserted! Something went wrong.");
                    }
                }
            }
        });

        btnCancelAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llEdit.setVisibility(View.GONE);
                llUpdate.setVisibility(View.VISIBLE);
                etFirst.setVisibility(View.VISIBLE);
                etLast.setVisibility(View.VISIBLE);
                etPhone.setVisibility(View.VISIBLE);

                etFirst.setText(contact.firstName);
                etLast.setText(contact.lastName);
                etPhone.setText(contact.phone);
            }
        });

        btnCancelEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etFirst.setText(null);
                etLast.setText(null);
                etPhone.setText(null);

                etFirst.setVisibility(View.GONE);
                etLast.setVisibility(View.GONE);
                etPhone.setVisibility(View.GONE);
                llUpdate.setVisibility(View.GONE);
                llEdit.setVisibility(View.VISIBLE);
            }
        });

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etFirst.getText() != null &&
                        etLast.getText() != null &&
                        etPhone.getText() != null) {
                    ContactDao contactDao = new ContactDao(getApplicationContext(),
                            contact.id,
                            etFirst.getText().toString(),
                            etLast.getText().toString(),
                            etPhone.getText().toString());
                    long result = contactDao.update();
                    if(result > 0) {
                        Toast.makeText(getApplicationContext(),"Contact updated.", Toast.LENGTH_LONG).show();
                        Log.d(TAG, "Contact updated.");
                        finish();
                    } else {
                        Toast.makeText(getApplicationContext(),
                                "Contact not was updated! Something went wrong.",
                                Toast.LENGTH_LONG).show();
                        Log.e(TAG, "Contact was not updated! Something went wrong.");
                    }
                }
            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showConfirmDialog();
            }
        });
    }

    @Override
    protected void onStop(){
        super.onStop();
        finish();
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {
        ContactDao contactDao = new ContactDao(getApplicationContext(), contact.id,
                "", "", "");
        long result = contactDao.delete();
        if(result > 0) {
            Toast.makeText(getApplicationContext(),"Contact deleted.", Toast.LENGTH_LONG).show();
            Log.d(TAG, "Contact deleted.");
            finish();
        } else {
            Toast.makeText(getApplicationContext(),
                    "Contact was not deleted! Something went wrong.",
                    Toast.LENGTH_LONG).show();
            Log.e(TAG, "Contact was not deleted! Something went wrong.");
        }
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {
        Log.d(TAG, "Contact deleted operation canceled.");
    }

    public void showConfirmDialog() {
        DialogFragment dialog = new ConfirmDialog();
        dialog.show(getSupportFragmentManager(), "CONFIRM");
    }
}