package com.behmerd.bussinessaction.database.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import androidx.annotation.NonNull;

import com.behmerd.bussinessaction.database.DBHelper;
import com.behmerd.bussinessaction.database.DBHelper.DatabaseContract.ContactEntry;
import com.behmerd.bussinessaction.model.Contact;

public class ContactDao extends Contact implements Dao {
    private Context context;
    private final String TAG = "BA_CONTACT_DAO";
    DBHelper dbHelper;

    public ContactDao(@NonNull Context context){
        this.context = context;
        this.dbHelper = new DBHelper(context);
    }

    public ContactDao(@NonNull Context context, @NonNull String firstName,
                      @NonNull String lastName, @NonNull String phone){
        super(firstName, lastName, phone);
        this.context = context;
        this.dbHelper = new DBHelper(context);
    }

    public ContactDao(@NonNull Context context, long id, @NonNull String firstName,
                      @NonNull String lastName, @NonNull String phone){
        super(id, firstName, lastName, phone);
        this.context = context;
        this.dbHelper = new DBHelper(context);
    }

    @Override
    public long insert() {
        try {
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(ContactEntry.FIRSTNAME, this.firstName);
            values.put(ContactEntry.LASTNAME, this.lastName);
            values.put(ContactEntry.PHONE, this.phone);
            long result = db.insert(ContactEntry.TABLE_NAME,
                    null, values);
            Log.d(TAG, String.format("Contact insertion result: %d", result));
            return result;
        } catch (Exception e) {
            Log.e(TAG, "Contact insert operation failed: " + e.getMessage());
            return -1;
        }
    }

    @Override
    public long update() {
        try {
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(ContactEntry.FIRSTNAME, this.firstName);
            values.put(ContactEntry.LASTNAME, this.lastName);
            values.put(ContactEntry.PHONE, this.phone);

            String selection = ContactEntry._ID + " = ?";
            String[] selectionArgs = {String.valueOf(this.id)};

            long result = db.update(ContactEntry.TABLE_NAME,
                    values,
                    selection,
                    selectionArgs);
            Log.d(TAG, String.format("Contact update result: %d", result));
            return result;
        } catch (Exception e) {
            Log.e(TAG, "Contact update operation failed: " + e.getMessage());
            return -1;
        }
    }

    @Override
    public long delete() {
        try {
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            String selection = ContactEntry._ID + " = ?";
            String[] selectionArgs = { String.valueOf(this.id)};
            long result = db.delete(ContactEntry.TABLE_NAME, selection, selectionArgs);
            Log.d(TAG, String.format("Contact deletion result: %d", result));
            return result;
        } catch (Exception e) {
            Log.e(TAG, "Contact delete operation failed: " + e.getMessage());
            return -1;
        }
    }

    @Override
    public Cursor select() {

        try {
            SQLiteDatabase db = dbHelper.getReadableDatabase();
            String[] projection = {
                    ContactEntry._ID,
                    ContactEntry.FIRSTNAME,
                    ContactEntry.LASTNAME,
                    ContactEntry.PHONE
            };

            return db.query(
                    ContactEntry.TABLE_NAME,
                    projection,
                    super.selection,
                    super.selectionArgs,
                    super.groupBy,
                    null,
                    super.orderBy
            );
        } catch (Exception e) {
            Log.e(TAG, "Contact select operation failed: " + e.getMessage());
            return null;
        }
    }
}
