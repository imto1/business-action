package com.behmerd.bussinessaction.database.dao;


import android.database.Cursor;

public interface Dao {
    public long insert();

    public long update();

    public long delete();

    public Cursor select();
}
