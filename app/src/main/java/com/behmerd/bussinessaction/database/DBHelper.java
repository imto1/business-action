package com.behmerd.bussinessaction.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

public class DBHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "database.db";

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DatabaseContract.SQL_CREATE_CONTACT_ENTRY);
        db.execSQL(DatabaseContract.SQL_CREATE_CONTACT_NAME_INDEX);
        db.execSQL(DatabaseContract.SQL_CREATE_CONTACT_PHONE_INDEX);

        db.execSQL(DatabaseContract.SQL_CREATE_SMS_ENTRY);
        db.execSQL(DatabaseContract.SQL_CREATE_SMS_INDEX);

        db.execSQL(DatabaseContract.SQL_CREATE_RECIPIENT_ENTRY);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DatabaseContract.SQL_DELETE_CONTACT_ENTRY);
        db.execSQL(DatabaseContract.SQL_DELETE_CONTACT_NAME_INDEX);
        db.execSQL(DatabaseContract.SQL_DELETE_CONTACT_PHONE_INDEX);

        db.execSQL(DatabaseContract.SQL_DELETE_SMS_ENTRY);
        db.execSQL(DatabaseContract.SQL_DELETE_SMS_INDEX);

        db.execSQL(DatabaseContract.SQL_DELETE_WELCOME_ENTRY);

        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    public final class DatabaseContract {
        private DatabaseContract() {
        }

        // Contact Entry
        public class ContactEntry implements BaseColumns {
            public static final String TABLE_NAME = "contact";
            public static final String FIRSTNAME = "first_name";
            public static final String LASTNAME = "last_name";
            public static final String PHONE = "phone";
        }

        private static final String SQL_CREATE_CONTACT_ENTRY =
                "CREATE TABLE IF NOT EXISTS " + ContactEntry.TABLE_NAME + " (" +
                        ContactEntry._ID + " INTEGER PRIMARY KEY," +
                        ContactEntry.FIRSTNAME + " TEXT," +
                        ContactEntry.LASTNAME + " TEXT," +
                        ContactEntry.PHONE + " TEXT UNIQUE)";

        private static final String SQL_CREATE_CONTACT_NAME_INDEX =
                "CREATE INDEX IF NOT EXISTS " + ContactEntry.TABLE_NAME + "_name ON " +
                        ContactEntry.TABLE_NAME + "(" + ContactEntry.FIRSTNAME +
                        "," + ContactEntry.LASTNAME + ")";

        private static final String SQL_CREATE_CONTACT_PHONE_INDEX =
                "CREATE UNIQUE INDEX IF NOT EXISTS " + ContactEntry.TABLE_NAME + "_" +
                        ContactEntry.PHONE + " ON " +
                        ContactEntry.TABLE_NAME + "(" + ContactEntry.PHONE + ")";

        private static final String SQL_DELETE_CONTACT_ENTRY =
                "DROP TABLE IF EXISTS " + ContactEntry.TABLE_NAME;

        private static final String SQL_DELETE_CONTACT_NAME_INDEX =
                "DROP INDEX IF EXISTS " + ContactEntry.TABLE_NAME + "_name";

        private static final String SQL_DELETE_CONTACT_PHONE_INDEX =
                "DROP INDEX IF EXISTS " + ContactEntry.TABLE_NAME + "_" + ContactEntry.PHONE;


        // SMS Entry
        public class SMSEntry implements BaseColumns{
            public static final String TABLE_NAME = "sms";
            public static final String SENDER = "sender";
            public static final String TIMESTAMP = "timestamp";
            public static final String BODY = "body";
        }

        private static final String SQL_CREATE_SMS_ENTRY =
                "CREATE TABLE IF NOT EXISTS " + SMSEntry.TABLE_NAME + " (" +
                        SMSEntry._ID + " INTEGER PRIMARY KEY," +
                        SMSEntry.SENDER + " INTEGER," +
                        SMSEntry.TIMESTAMP + " INTEGER," +
                        SMSEntry.BODY + " TEXT," +
                        "FOREIGN KEY(" + SMSEntry.SENDER + ") REFERENCES " +
                        ContactEntry.TABLE_NAME + "(" + ContactEntry._ID +
                        ") ON DELETE CASCADE ON UPDATE CASCADE)";

        private static final String SQL_CREATE_SMS_INDEX =
                "CREATE INDEX IF NOT EXISTS " + SMSEntry.TABLE_NAME + "_index ON " +
                        SMSEntry.TABLE_NAME + "(" + SMSEntry.SENDER + "," +
                        SMSEntry.BODY + ")";

        private static final String SQL_DELETE_SMS_ENTRY =
                "DROP TABLE IF EXISTS " + SMSEntry.TABLE_NAME;

        private static final String SQL_DELETE_SMS_INDEX =
                "DROP INDEX IF EXISTS " + SMSEntry.TABLE_NAME + "_index";


        // Recipient Entry
        public class RecipientEntry implements BaseColumns{
            public static final String TABLE_NAME = "recipient";
            public static final String RECEIVER = "receiver";
        }

        private static final String SQL_CREATE_RECIPIENT_ENTRY =
                "CREATE TABLE IF NOT EXISTS " + RecipientEntry.TABLE_NAME + " (" +
                        RecipientEntry._ID + " INTEGER PRIMARY KEY," +
                        RecipientEntry.RECEIVER + " INTEGER UNIQUE," +
                        "FOREIGN KEY(" + RecipientEntry.RECEIVER + ") REFERENCES " +
                        ContactEntry.TABLE_NAME + "(" + ContactEntry._ID +
                        ") ON DELETE CASCADE ON UPDATE CASCADE)";

        private static final String SQL_DELETE_WELCOME_ENTRY =
                "DROP TABLE IF EXISTS " + RecipientEntry.TABLE_NAME;
    }
}
