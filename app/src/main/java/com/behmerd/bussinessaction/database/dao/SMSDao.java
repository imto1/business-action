package com.behmerd.bussinessaction.database.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import androidx.annotation.NonNull;

import com.behmerd.bussinessaction.database.DBHelper;
import com.behmerd.bussinessaction.database.DBHelper.DatabaseContract.SMSEntry;
import com.behmerd.bussinessaction.model.SMS;

public class SMSDao extends SMS implements Dao{
    private Context context;
    private final String TAG = "BA_SMS_DAO";
    DBHelper dbHelper;

    public SMSDao(@NonNull Context context) {
        this.context = context;
        this.dbHelper = new DBHelper(context);
    }
    public SMSDao(Context context, long id, long sender,
                  @NonNull long timeStamp, @NonNull String body) {
        super(id, sender, timeStamp, body);
        this.context = context;
        this.dbHelper = new DBHelper(context);
    }

    public SMSDao(Context context, long sender,
                  @NonNull long timeStamp, @NonNull String body) {
        super(sender, timeStamp, body);
        this.context = context;
        this.dbHelper = new DBHelper(context);
    }

    @Override
    public long insert() {
        try {
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(SMSEntry.SENDER, this.sender);
            values.put(SMSEntry.TIMESTAMP, this.timeStamp);
            values.put(SMSEntry.BODY, this.body);
            long result = db.insert(SMSEntry.TABLE_NAME,
                    null, values);
            Log.d(TAG, String.format("SMS insertion result: %d", result));
            return result;
        } catch (Exception e) {
            Log.e(TAG, "SMS insert operation failed: " + e.getMessage());
            return -1;
        }
    }

    @Override
    public long update() {
        try {
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(SMSEntry.SENDER, this.sender);
            values.put(SMSEntry.TIMESTAMP, this.timeStamp);
            values.put(SMSEntry.BODY, this.body);

            String selection = SMSEntry._ID + " = ?";
            String[] selectionArgs = {String.valueOf(this.id)};

            long result = db.update(SMSEntry.TABLE_NAME,
                    values,
                    selection,
                    selectionArgs);
            Log.d(TAG, String.format("SMS update result: %d", result));
            return result;
        } catch (Exception e) {
            Log.e(TAG, "SMS update operation failed: " + e.getMessage());
            return -1;
        }
    }

    @Override
    public long delete() {
        try {
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            String selection = SMSEntry._ID + " = ?";
            String[] selectionArgs = { String.valueOf(this.id)};
            long result = db.delete(SMSEntry.TABLE_NAME, selection, selectionArgs);
            Log.d(TAG, String.format("SMS deletion result: %d", result));
            return result;
        } catch (Exception e) {
            Log.e(TAG, "SMS delete operation failed: " + e.getMessage());
            return -1;
        }
    }

    @Override
    public Cursor select() {
        Cursor cursor = null;

        try {
            SQLiteDatabase db = dbHelper.getReadableDatabase();
            String[] projection = {
                    SMSEntry._ID,
                    SMSEntry.SENDER,
                    SMSEntry.TIMESTAMP,
                    SMSEntry.BODY
            };

            return cursor = db.query(
                    SMSEntry.TABLE_NAME,
                    projection,
                    super.selection,
                    super.selectionArgs,
                    super.groupBy,
                    null,
                    super.orderBy
            );
        } catch (Exception e) {
            Log.e(TAG, "SMS select operation failed: " + e.getMessage());
            return null;
        } finally {
            if(cursor != null) cursor.close();
        }
    }
}
