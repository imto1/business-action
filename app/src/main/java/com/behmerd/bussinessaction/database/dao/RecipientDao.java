package com.behmerd.bussinessaction.database.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import androidx.annotation.NonNull;

import com.behmerd.bussinessaction.database.DBHelper;
import com.behmerd.bussinessaction.database.DBHelper.DatabaseContract.RecipientEntry;
import com.behmerd.bussinessaction.model.Recipient;

public class RecipientDao extends Recipient implements Dao{
    private Context context;
    private final String TAG = "BA_RECIPIENT_DAO";
    DBHelper dbHelper;

    public RecipientDao(Context context){
        this.context =context;
        this.dbHelper = new DBHelper(context);
    }

    public RecipientDao(Context context, @NonNull long receiver){
        super(receiver);
        this.context =context;
        this.dbHelper = new DBHelper(context);
    }

    public RecipientDao(Context context, long id, long receiver){
        super(id, receiver);
        this.context =context;
        this.dbHelper = new DBHelper(context);
    }

    @Override
    public long insert() {
        try {
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(RecipientEntry.RECEIVER, this.receiver);
            long result = db.insert(RecipientEntry.TABLE_NAME,
                    null, values);
            Log.d(TAG, String.format("Recipient insertion result: %d", result));
            return result;
        } catch (Exception e) {
            Log.e(TAG, "Recipient item insert operation failed: " + e.getMessage());
            return -1;
        }
    }

    @Override
    public long update() {
        try {
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(RecipientEntry.RECEIVER, this.receiver);

            String selection = RecipientEntry._ID + " = ?";
            String[] selectionArgs = {String.valueOf(this.id)};

            long result = db.update(RecipientEntry.TABLE_NAME,
                    values,
                    selection,
                    selectionArgs);
            Log.d(TAG, String.format("Recipient update result: %d", result));
            return result;
        } catch (Exception e) {
            Log.e(TAG, "Recipient item update operation failed: " + e.getMessage());
            return -1;
        }
    }

    @Override
    public long delete() {
        try {
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            String selection = RecipientEntry._ID + " = ?";
            String[] selectionArgs = { String.valueOf(this.id)};
            long result = db.delete(RecipientEntry.TABLE_NAME, selection, selectionArgs);
            Log.d(TAG, String.format("Recipient deletion result: %d", result));
            return result;
        } catch (Exception e) {
            Log.e(TAG, "Recipient item delete operation failed: " + e.getMessage());
            return -1;
        }
    }

    @Override
    public Cursor select() {
        try {
            SQLiteDatabase db = dbHelper.getReadableDatabase();
            String[] projection = {
                    RecipientEntry._ID,
                    RecipientEntry.RECEIVER
            };

            return db.query(
                    RecipientEntry.TABLE_NAME,
                    projection,
                    super.selection,
                    super.selectionArgs,
                    super.groupBy,
                    null,
                    super.orderBy
            );
        } catch (Exception e) {
            Log.e(TAG, "Recipient item select operation failed: " + e.getMessage());
            return null;
        }
    }
}
