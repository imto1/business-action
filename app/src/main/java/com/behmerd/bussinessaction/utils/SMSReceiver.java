package com.behmerd.bussinessaction.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;

import com.behmerd.bussinessaction.utils.SMSListener;

public class SMSReceiver extends BroadcastReceiver {
    private final String TAG = "BA_SMSReceiver";
    private static SMSListener mListener;

    // TODO standardize sender's phone no.
    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")){
            Bundle bundle = intent.getExtras();
            SmsMessage[] smsMessage = null;
            if (bundle != null){
                //---retrieve the SMS message received---
                String message = "";
                String sender = "";
                long timestamp = 0;
                try{
                    Object[] pdus = (Object[]) bundle.get("pdus");
                    smsMessage = new SmsMessage[pdus.length];
                    for(int i=0;i<smsMessage.length;i++){
                        System.out.println(i);
                        smsMessage[i] = SmsMessage.createFromPdu((byte[])pdus[i],
                                bundle.getString("format"));
                        sender = smsMessage[i].getOriginatingAddress();
                        message += smsMessage[i].getMessageBody();
                        timestamp = smsMessage[i].getTimestampMillis();
                    }
                    mListener.SMSReceived(sender, timestamp, message);
                } catch(Exception e) {
                    Log.e(TAG,"SMS receive operation failed: " + e.getMessage());
                }
            }
        }
    }

    public static void bindListener(SMSListener listener){
        mListener = listener;
    }
}
