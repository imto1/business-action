package com.behmerd.bussinessaction.utils;

public interface SMSListener {
    /**
     * To call this method when new message received and send back
     * @param message Message
     */
    void SMSReceived(String sender, long timestamp, String message);
}
