package com.behmerd.bussinessaction.utils;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.app.Notification;
import android.app.PendingIntent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.telephony.SmsManager;
import android.text.format.DateFormat;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;

import com.behmerd.bussinessaction.MainActivity;
import com.behmerd.bussinessaction.R;
import com.behmerd.bussinessaction.database.DBHelper.DatabaseContract.RecipientEntry;
import com.behmerd.bussinessaction.database.DBHelper.DatabaseContract.ContactEntry;
import com.behmerd.bussinessaction.database.dao.ContactDao;
import com.behmerd.bussinessaction.database.dao.SMSDao;
import com.behmerd.bussinessaction.database.dao.RecipientDao;
import com.behmerd.bussinessaction.model.Contact;
import com.behmerd.bussinessaction.model.Recipient;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class FGService extends Service implements SMSListener {

    private static final String TAG = "BA_FOREGROUND_SERVICE";
    public static final String ACTION_START_FOREGROUND_SERVICE = "ACTION_START_FOREGROUND_SERVICE";
    public static final String ACTION_STOP_FOREGROUND_SERVICE = "ACTION_STOP_FOREGROUND_SERVICE";

    //runs without a timer by reposting this handler at the end of the runnable
    Handler timerHandler = new Handler();
    Runnable timerRunnable = new Runnable() {
        @Override
        public void run() {
            Preferences preferences = new Preferences(getApplicationContext());
            try {
                RecipientDao recipientDao = new RecipientDao(getApplicationContext());
                Cursor recipientCursor = recipientDao.select();
                Log.d(TAG, String.format("Select new recipients: %d item(s).", recipientCursor.getCount()));

                if(recipientCursor.getCount() > 0) {
                    List<Recipient> recipients = new ArrayList<>();

                    while (recipientCursor.moveToNext()) {
                        recipients.add(new Recipient(
                                recipientCursor.getLong(recipientCursor.getColumnIndexOrThrow(RecipientEntry._ID)),
                                recipientCursor.getLong(recipientCursor.getColumnIndexOrThrow(RecipientEntry.RECEIVER))
                        ));
                    }
                    recipientCursor.close();

                    ContactDao contactDao = new ContactDao(getApplicationContext());

                    StringBuilder receivers = new StringBuilder("");
                    StringBuilder ids = new StringBuilder("");
                    for (Recipient recipient : recipients) {
                        receivers.append(ContactEntry._ID).append(" = (?) OR ");
                        ids.append(recipient.receiver).append(",");
                    }
                    System.out.println(receivers.lastIndexOf("OR "));
                    System.out.println(receivers.length());
                    receivers.delete(receivers.lastIndexOf(" OR "), receivers.length());
                    ids.deleteCharAt(ids.lastIndexOf(","));

                    contactDao.selection = receivers.toString();
                    contactDao.selectionArgs = ids.toString().split(",");
                    Cursor contactCursor = contactDao.select();
                    Log.d(TAG, String.format("Select contacts: %d item(s).", contactCursor.getCount()));

                    if(contactCursor.getCount() > 0) {
                        List<Contact> contacts = new ArrayList<>();

                        while (contactCursor.moveToNext())
                            contacts.add(new Contact(
                                    contactCursor.getLong(contactCursor.getColumnIndexOrThrow(ContactEntry._ID)),
                                    contactCursor.getString(contactCursor.getColumnIndexOrThrow(ContactEntry.FIRSTNAME)),
                                    contactCursor.getString(contactCursor.getColumnIndexOrThrow(ContactEntry.LASTNAME)),
                                    contactCursor.getString(contactCursor.getColumnIndexOrThrow(ContactEntry.PHONE))
                            ));
                        contactCursor.close();

                        String message = preferences.getPreference(preferences.WELCOME_MESSAGE);

                        for (Contact contact : contacts)
                            sendSMS(contact.phone, message);
                        Log.d(TAG, String.format("Sending message to %d recipient(s) done.", contacts.size()));

                        for (Recipient recipient : recipients) {
                            recipientDao = new RecipientDao(getApplicationContext(),
                                    recipient.id, recipient.receiver);
                            recipientDao.delete();
                        }
                        Log.d(TAG, String.format("Clearing %d recipient(s) from Database done.", recipients.size()));
                    } else
                        Log.d(TAG, "There is no contacts.");
                } else
                    Log.d(TAG, "There is nothing to do.");
            } catch (Exception e) {
                Log.e(TAG, "Send SMS operation failed: " + e.getMessage());
            } finally {
                long delay = Long.parseLong(preferences.getPreference(preferences.DELAY_TIME));
                delay = (delay * 1000) * 60;
                timerHandler.postDelayed(this, delay);
            }
        }
    };

    public FGService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if(intent != null)
        {
            String action = intent.getAction();
            switch (action)
            {
                case ACTION_START_FOREGROUND_SERVICE:
                    startForegroundService();
                    Toast.makeText(getApplicationContext(), "Foreground service is started.",
                            Toast.LENGTH_LONG).show();
                    break;
                case ACTION_STOP_FOREGROUND_SERVICE:
                    stopForegroundService();
                    Toast.makeText(getApplicationContext(), "Foreground service is stopped.",
                            Toast.LENGTH_LONG).show();
                    break;
            }
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        timerHandler.removeCallbacks(timerRunnable);
    }

    private void startForegroundService() {
        String NOTIFICATION_CHANNEL_ID = "com.behmerd.bussinessaction.service";
        String CHANNEL_NAME = "BUSINESS_ACTION";

        NotificationChannel channel = new NotificationChannel(NOTIFICATION_CHANNEL_ID,
                CHANNEL_NAME, NotificationManager.IMPORTANCE_DEFAULT);
        channel.setLightColor(Color.BLUE);
        channel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        assert manager != null;
        manager.createNotificationChannel(channel);

        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);
        Bitmap largeIconBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.ic_coffecup);

        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);

        // Set big text style.
        notificationBuilder.setWhen(System.currentTimeMillis())
                .setSmallIcon(R.drawable.ic_coffecup)
                .setLargeIcon(largeIconBitmap)
                .setFullScreenIntent(pendingIntent, true);

        Notification notification = notificationBuilder.setOngoing(true)
                .setSmallIcon(R.drawable.ic_coffecup)
                .setContentTitle("Handling messages")
                .setPriority(NotificationManager.IMPORTANCE_MIN)
                .setCategory(Notification.CATEGORY_SERVICE)
                .setContentIntent(pendingIntent)
                .build();

        startForeground(1, notification);

        SMSReceiver.bindListener(this);

        timerHandler.postDelayed(timerRunnable, 0);
        Log.d(TAG, "Start foreground service.");
    }

    private void stopForegroundService()
    {
        timerHandler.removeCallbacks(timerRunnable);
        Log.d(TAG, "Stop foreground service.");
        // Stop foreground service and remove the notification.
        stopForeground(true);
        // Stop the foreground service.
        stopSelf();
    }

    private void sendSMS(String number, String message){
        SmsManager smsManager = SmsManager.getDefault();
        smsManager.sendTextMessage(number, null, message, null, null);
        Log.d(TAG, String.format("Message sent to %s", number));
    }

    @Override
    public void SMSReceived(String sender, long timestamp, String message) {
        ContactDao contactDao = new ContactDao(getApplicationContext());
        contactDao.selection = ContactEntry.PHONE + " LIKE ?";
        contactDao.selectionArgs = new String[]{sender};

        Cursor cursor = contactDao.select();

        if(cursor != null) {
            cursor.moveToNext();
            contactDao.id = cursor.getLong(cursor.getColumnIndexOrThrow(ContactEntry._ID));
            contactDao.firstName = cursor.getString(cursor.getColumnIndexOrThrow(ContactEntry.FIRSTNAME));
            contactDao.lastName = cursor.getString(cursor.getColumnIndexOrThrow(ContactEntry.LASTNAME));
            contactDao.phone = cursor.getString(cursor.getColumnIndexOrThrow(ContactEntry.PHONE));
            SMSDao smsDao = new SMSDao(getApplicationContext(), contactDao.id, timestamp, message);
            Toast.makeText(this, String.format("New Message Received: %s %s",
                    contactDao.firstName, contactDao.lastName), Toast.LENGTH_SHORT).show();
            Log.d(TAG, String.format("New Message Received: %s %s",
                    contactDao.firstName, contactDao.lastName));

            long result = smsDao.insert();
            Log.d(TAG, String.format("New SMS record inserted: %d", result));

            cursor.close();
        } else
            Log.d(TAG, String.format("Received message is not from registered customers: %s @ %s",
                    sender, getDate(timestamp)));
    }

    private String getDate(long time) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time * 1000);
        return DateFormat.format("yyyy-MM-dd HH:mm:ss", cal).toString();
    }
}
