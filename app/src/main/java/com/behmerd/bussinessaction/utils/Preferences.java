package com.behmerd.bussinessaction.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import androidx.annotation.NonNull;

public class Preferences {
    private final Context context;

    private final String TAG = "SHARED_PREFERENCES";
    private final String PREFERENCE_NAME = "QUICK_ACTION_PREFERENCES";
    public final String DELAY_TIME = "DELAY_TIME";
    public final String WELCOME_MESSAGE = "WELCOME_MESSAGE";
    public final String INIT_SETTING = "INIT_SETTING";

    public Preferences(Context context){
        this.context = context;
    }

    public boolean setPreference(@NonNull String key, @NonNull String data){
        try {
            SharedPreferences sp = context.getSharedPreferences(PREFERENCE_NAME,
                    Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sp.edit();
            editor.putString(key, data);
            return editor.commit();
        } catch (Exception e) {
            Log.d(TAG, String.format("Setting preference failed: /%s", e.getMessage()));
        }
        return false;
    }

    public String getPreference(@NonNull String key){
        try {
            SharedPreferences sp = context.getSharedPreferences(PREFERENCE_NAME,
                    Context.MODE_PRIVATE);
            return sp.getString(key, null);
        } catch (Exception e){
            Log.d(TAG, String.format("Getting preference failed: /%s", e.getMessage()));
        }
        return null;
    }

    public boolean clearPreference(){
        try {
            SharedPreferences sp = context.getSharedPreferences(PREFERENCE_NAME,
                    Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sp.edit();
            editor.clear();
            return editor.commit();
        } catch (Exception e) {
            Log.d(TAG, String.format("Clearing preferences failed: /%s", e.getMessage()));
        }
        return false;
    }
}
